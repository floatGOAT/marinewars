package AI;

import game.IPlayer;
import game.Ship;

import java.util.ArrayList;
import java.util.List;

/**
 * User: Shavanachola@gmail.com
 * Name: Andrew Alexeev
 * Date: 18.10.2018
 * Time: 20:43
 */

//TODO: create AI part of the app
public class MarinerBot implements IPlayer {
    private List<Ship> ships;

    public MarinerBot() {
        ships = new ArrayList<>();
    }

    @Override
    public void setShips(List<Ship> ships) {
        this.ships = ships;
    }

    @Override
    public List<Ship> getShips() {
        return ships;
    }
}
