package app;

import game.Game;
import game.GameMode;

/**
 * User: Shavanachola@gmail.com
 * Name: Andrew Alexeev
 * Date: 20.10.2018
 * Time: 21:58
 */

public class App {
    public static GameMode GAME_MOD = GameMode.CLASSIC;

    private Game currentGame;

    private static App instance;

    public static App getInstance() {
        if (instance == null) {
            instance = new App();
        }
        return instance;
    }

    private App() {}

    public Game getCurrentGame() {
        return currentGame;
    }

    public void setCurrentGame(Game currentGame) {
        this.currentGame = currentGame;
    }

}
