package app;

import UI.RootPane;
import game.Game;
import game.GameMode;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class MarineWars extends Application {
    private Stage mainStage;

    private RootPane rootPane;

    /***************************************************************
     *
     * Public methods
     *
     ***************************************************************/

    public Stage getMainStage() {
        return mainStage;
    }

    /***************************************************************
     *
     * Application entry point
     *
     ***************************************************************/

    @Override
    public void start(Stage primaryStage) {
        mainStage = primaryStage;
        primaryStage.setTitle("Marine wars");

        rootPane = RootPane.getInstance();
        mainStage.setScene(new Scene(rootPane.getView(), 700,550));
//        mainStage.setMaximized(true);
        mainStage.show();
    }


    public static void main(String[] args) {
        launch(args);
    }
}
