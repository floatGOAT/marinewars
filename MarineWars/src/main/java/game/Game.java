package game;

import AI.MarinerBot;
import UI.RootPane;
import UI.UIShip;
import app.App;
import javafx.scene.layout.Pane;

import java.util.ArrayList;
import java.util.List;

/**
 * User: Shavanachola@gmail.com
 * Name: Andrew Alexeev
 * Date: 20.10.2018
 * Time: 21:26
 */

public class Game {
    private IPlayer player1;
    private IPlayer player2;

    public Game() {
        player1 = new Player();
        player2 = new MarinerBot();

        prepareShips();
        fillShipPicker(player1.getShips());
    }

    private void prepareShips() {
        switch (App.GAME_MOD) {
            case CLASSIC:
                //Ships for player1
                List<Ship> shipsP1 = new ArrayList<>();
                for (int i = 4; i > 0; i--) {
                    for (int j = i; j > 0; j--) {
                        Ship newShip = new Ship(5 - i);
                        shipsP1.add(newShip);
                    }
                }
                player1.setShips(shipsP1);

                //Ships for player2
                List<Ship> shipsP2 = new ArrayList<>();
                for (int i = 4; i > 0; i--) {
                    for (int j = i; j > 0; j--) {
                        Ship newShip = new Ship(5 - i);
                        shipsP1.add(newShip);
                    }
                }
                player2.setShips(shipsP1);
                break;
            default:
                break;
        }
    }

    private void fillShipPicker(List<Ship> ships) {
        Pane shipPicker = RootPane.getInstance().getController().getShipPicker();
        ships.forEach(ship -> {
            UIShip newShip = new UIShip(ship, UIShip.ShipOrientation.VERTICAL);
            shipPicker.getChildren().add(newShip);
        });
    }

}
