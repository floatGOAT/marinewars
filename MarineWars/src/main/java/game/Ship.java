package game;

/**
 * User: Shavanachola@gmail.com
 * Name: Andrew Alexeev
 * Date: 20.10.2018
 * Time: 19:55
 */

public class Ship {
    private final int size;

    public Ship(int size) {
        this.size = size;

    }

    public int getSize() {
        return size;
    }

}
