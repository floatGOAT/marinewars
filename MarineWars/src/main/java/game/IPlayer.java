package game;

import java.util.List;

public interface IPlayer {
    void setShips(List<Ship> ships);

    List<Ship> getShips();
}
