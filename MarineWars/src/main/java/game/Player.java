package game;

import java.util.ArrayList;
import java.util.List;

/**
 * User: Shavanachola@gmail.com
 * Name: Andrew Alexeev
 * Date: 20.10.2018
 * Time: 22:03
 */

public class Player implements IPlayer {
    private List<Ship> ships;

    public Player() {
        ships = new ArrayList<>();
    }

    @Override
    public List<Ship> getShips() {
        return ships;
    }

    @Override
    public void setShips(List<Ship> ships) {
        this.ships = ships;
    }
}
