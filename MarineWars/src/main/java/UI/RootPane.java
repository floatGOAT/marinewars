package UI;

import UI.controllers.RootPaneController;
import javafx.fxml.FXMLLoader;
import javafx.scene.layout.BorderPane;

import java.io.IOException;

/**
 * User: Shavanachola@gmail.com
 * Name: Andrew Alexeev
 * Date: 19.10.2018
 * Time: 10:19
 */

public class RootPane {
    private BorderPane view;

    private RootPaneController controller;

    private Gameboard gameboard;

    private int scale = 50;

    private static RootPane instance;

    public static RootPane getInstance() {
        if (instance == null) {
            instance = new RootPane();
        }
        return instance;
    }

    /***************************************************************
     *
     * Instance creation
     *
     ***************************************************************/

    private RootPane() {
        initializeFxml();

        gameboard = new Gameboard();
        controller.setRootCenterGameboard(gameboard.getView());
    }

    private void initializeFxml() {
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/app/RootPane.fxml"));
            view = loader.load();
            controller = loader.getController();
        } catch (IOException e) {
            //TODO: handle user-friendly app abort
            e.printStackTrace();
        }
    }

    /***************************************************************
     *
     * Public methods
     *
     ***************************************************************/

    public BorderPane getView() {
        return view;
    }

    public RootPaneController getController() {
        return controller;
    }

    public int getScale() {
        return scale;
    }
}
