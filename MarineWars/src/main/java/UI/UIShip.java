package UI;

import game.Ship;
import javafx.scene.image.Image;
import javafx.scene.input.ClipboardContent;
import javafx.scene.input.DataFormat;
import javafx.scene.input.Dragboard;
import javafx.scene.input.TransferMode;
import javafx.scene.shape.Rectangle;
import javafx.scene.transform.Rotate;

/**
 * User: Shavanachola@gmail.com
 * Name: Andrew Alexeev
 * Date: 20.10.2018
 * Time: 19:41
 */

public class UIShip extends Rectangle {
    public enum ShipOrientation {
        VERTICAL,
        HORIZONTAL
    }

    public static DataFormat integerDataFormat = new DataFormat("java.lang.Integer");

    private Ship ship;

    private ShipOrientation orientation;

    private int scale;


    public UIShip(Ship ship, ShipOrientation orientation) {
        this.ship = ship;
        this.orientation = orientation;
        this.scale = RootPane.getInstance().getScale();

        initializeView();

        initializeEvents();
    }

    private void initializeView() {
        setHeight(scale * ship.getSize());
        setWidth(scale);
        if (orientation == ShipOrientation.HORIZONTAL) {
            setRotate(90.0);
        }

        getStyleClass().add("ui-ship");
    }


    private void initializeEvents() {
        setOnMouseClicked(event -> {
            setRotate(getRotate() + 90.0);
        });

        setOnDragDetected(event -> {
            event.consume();

            startFullDrag();
            Dragboard dragboard = startDragAndDrop(TransferMode.MOVE);
            dragboard.setDragView(new Image("/images/ship2.jpg", 50.0, 100.0, true, false), 25, 50);
            ClipboardContent content = new ClipboardContent();
            content.put(integerDataFormat, ship.getSize());
            dragboard.setContent(content);
        });
    }
}
