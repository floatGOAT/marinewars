package UI;

import UI.controllers.GameboardController;
import javafx.fxml.FXMLLoader;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;

import java.io.IOException;

/**
 * User: Shavanachola@gmail.com
 * Name: Andrew Alexeev
 * Date: 19.10.2018
 * Time: 11:18
 */

public class Gameboard {
    private AnchorPane view;

    private GameboardController controller;

    public Gameboard() {
        initializeFxml();
    }

    private void initializeFxml() {
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/UI/Gameboard.fxml"));
            view = loader.load();
            controller = loader.getController();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public AnchorPane getView() {
        return view;
    }

    public GameboardController getController() {
        return controller;
    }
}
