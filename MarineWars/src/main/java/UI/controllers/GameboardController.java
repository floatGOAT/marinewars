package UI.controllers;

/**
 * User: Shavanachola@gmail.com
 * Name: Andrew Alexeev
 * Date: 19.10.2018
 * Time: 11:24
 */

import UI.UIShip;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.input.DataFormat;
import javafx.scene.input.DragEvent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.AnchorPane;

import java.net.URL;
import java.util.ResourceBundle;

public class GameboardController implements Initializable{
    @FXML
    private AnchorPane gameboard;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        System.out.println("Gameboard initializator called");
    }

    @FXML
    public void onDragOver(DragEvent event) {
        event.consume();

        Dragboard dragboard = event.getDragboard();
        int shipSize = (Integer) dragboard.getContent(UIShip.integerDataFormat);
        System.out.println(String.format("%d x = %f; y = %f", shipSize, event.getX(), event.getY()));
        event.acceptTransferModes(TransferMode.MOVE);
    }
}
