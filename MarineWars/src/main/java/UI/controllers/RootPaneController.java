package UI.controllers;

import UI.RootPane;
import app.App;
import app.MarineWars;
import game.Game;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.MenuBar;
import javafx.scene.layout.*;

import java.net.URL;
import java.util.ResourceBundle;

import static game.GameMode.CLASSIC;

/**
 * User: Shavanachola@gmail.com
 * Name: Andrew Alexeev
 * Date: 18.10.2018
 * Time: 22:06
 */

public class RootPaneController implements Initializable {
    @FXML
    private VBox rootLeftPanel;

    @FXML
    private FlowPane shipPicker;

    @FXML
    private VBox rootRightPanel;

    @FXML
    private AnchorPane rootCenterGameboard;

    @FXML
    private MenuBar rootMenuBar;

    /***************************************************************
     *
     * Instance creation
     *
     ***************************************************************/

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        System.out.println("RootPaneController initializator called");
    }

    /***************************************************************
     *
     * FXML events
     *
     ***************************************************************/

    @FXML
    void onNewGame(ActionEvent event) {
        Game game = new Game();
        App.getInstance().setCurrentGame(game);
    }

    @FXML
    void onRestartGame(ActionEvent event) {

    }

    @FXML
    void onExit(ActionEvent event) {

    }

    /***************************************************************
     *
     * Public methods
     *
     ***************************************************************/

    public void setRootCenterGameboard(AnchorPane gameboard) {
        rootCenterGameboard.getChildren().add(gameboard);
    }

    public Pane getShipPicker() {
        return shipPicker;
    }

    /***************************************************************
     *
     * Private methods
     *
     ***************************************************************/

}
